import { Price } from './price';
import { Rental } from './rental';

export class Customer {
    public static named(name: string): Customer {
        return new Customer(name, []);
    }

    private constructor(private readonly _name: string, private readonly _rentals: Rental[]) {
    }

    public add(rental: Rental): Customer {
        return new Customer(this._name, [...this._rentals, rental]);
    }

    public addRental(arg: Rental) {
        this._rentals.push(arg);
    }

    public get name(): string {
        return this._name;
    }

    public totalAmount(): Price {
        return this._rentals
            .map((each) => each.charge())
            .reduce(Price.add, Price.euro(0));
    }

    public frequentRenterPoints(): number {
        return this._rentals
            .map((each) => each.frequentRenterPoints())
            .reduce((left, right) => left + right, 0);
    }

    public statement(): string {
        let result = "Rental Record for " + this.name + "\n";
        for (const each of this._rentals) {
            result += "\t" + each.movieTitle + "\t" + each.charge().render() + "\n";
        }
        result += "Amount owed is " + this.totalAmount().render() + "\n";
        result += "You earned " + this.frequentRenterPoints() + " frequent renter points";
        return result;
    }
}
