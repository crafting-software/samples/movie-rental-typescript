import { Duration } from './duration';
import { Price } from './price';
import { parse } from './pricing/dsl';
import { Pricing } from './pricing/pricing';

export interface PriceCode {
    chargeFor(duration: Duration): Price;
    frequentRenterPointsFor(duration: Duration): number;
}

class Childrens implements PriceCode {
    private static PRICING: Pricing = parse("costs 1.5€ for 3 days then costs 1.5€ per day");

    public chargeFor(duration: Duration): Price {
        return Childrens.PRICING.chargeFor(duration);
    }

    public frequentRenterPointsFor(duration: Duration): number {
        return 1;
    }
}

class NewRelease implements PriceCode {
    private static PRICING: Pricing = parse("costs 3€ per day");

    public chargeFor(duration: Duration): Price {
        return NewRelease.PRICING.chargeFor(duration);
    }

    public frequentRenterPointsFor(duration: Duration): number {
        return duration.days > 1 ? 2 : 1;
    }
}

class Regular implements PriceCode {
    private static PRICING: Pricing = parse("costs 2€ for 2 days then costs 1.5€ per day");

    public chargeFor(duration: Duration): Price {
        return Regular.PRICING.chargeFor(duration);
    }

    public frequentRenterPointsFor(duration: Duration): number {
        return 1;
    }
}

export class PriceCodes {
    public static Childrens: PriceCode = new Childrens();
    public static NewRelease: PriceCode = new NewRelease();
    public static Regular: PriceCode = new Regular();
}
