
export class Price {
    public static euro(value: number): Price {
        return new Price(value);
    }

    public static equals(left: Price, right: Price): boolean {
        return left.render() === right.render();
    }

    public static add(left: Price, right: Price): Price {
        return Price.euro(left.amount + right.amount);
    }

    public static mul(left: Price, right: number): Price {
        return Price.euro(left.amount * right);
    }

    private constructor(private readonly _value: number) {
    }

    public get amount(): number {
        return this._value;
    }

    public render(): string {
        return this._value.toFixed(1);
    }
}
