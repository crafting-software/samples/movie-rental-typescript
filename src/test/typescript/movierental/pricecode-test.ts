import * as fc from 'fast-check';

import { Duration } from '../../../main/typescript/movierental/duration';
import { Price } from '../../../main/typescript/movierental/price';
import { PriceCodes } from '../../../main/typescript/movierental/pricecode';

describe("PriceCode", () => {

    describe("for regulars", () => {
        const REGULAR = PriceCodes.Regular;

        it("should charge flat when less than 3 days", () => {
            expect(REGULAR.chargeFor(Duration.days(1))).toEqual(Price.euro(2.0));
            expect(REGULAR.chargeFor(Duration.days(2))).toEqual(Price.euro(2.0));
        });

        it("should charge proportionaly when more than 3 days", () => {
            expect(REGULAR.chargeFor(Duration.days(3))).toEqual(Price.euro(3.5));
            expect(REGULAR.chargeFor(Duration.days(4))).toEqual(Price.euro(5.0));
        });

        it("should charge positive prices", () => {
            fc.assert(fc.property(
                fc.nat().filter((v: number) => v >= 1),
                (days: number) => {
                    const amount = REGULAR.chargeFor(Duration.days(days)).amount;
                    expect(amount).toBeGreaterThan(0);
                }));
        });

        it("should give 1 frequent renter point regardless of days", () => {
            fc.assert(fc.property(
                fc.nat().filter((v: number) => v >= 1),
                (days: number) => {
                    expect(REGULAR.frequentRenterPointsFor(Duration.days(days))).toEqual(1);
                }));
        });
    });

    describe("for childrens", () => {
        const CHILDRENS = PriceCodes.Childrens;

        it("should charge flat when less than 4 days", () => {
            expect(CHILDRENS.chargeFor(Duration.days(1))).toEqual(Price.euro(1.5));
            expect(CHILDRENS.chargeFor(Duration.days(2))).toEqual(Price.euro(1.5));
            expect(CHILDRENS.chargeFor(Duration.days(3))).toEqual(Price.euro(1.5));
        });

        it("should charge proportionaly when more than 4 days", () => {
            expect(CHILDRENS.chargeFor(Duration.days(4))).toEqual(Price.euro(3.0));
            expect(CHILDRENS.chargeFor(Duration.days(5))).toEqual(Price.euro(4.5));
        });

        it("should give 1 frequent renter point regardless of days", () => {
            fc.assert(fc.property(
                fc.nat().filter((v: number) => v >= 1),
                (days: number) => {
                    expect(CHILDRENS.frequentRenterPointsFor(Duration.days(days))).toEqual(1);
                }));
        });
    });

    describe("for new releases", () => {
        const NEW_RELEASE = PriceCodes.NewRelease;

        it("should charge proportionaly", () => {
            expect(NEW_RELEASE.chargeFor(Duration.days(1))).toEqual(Price.euro(3.0));
            expect(NEW_RELEASE.chargeFor(Duration.days(2))).toEqual(Price.euro(6.0));
            expect(NEW_RELEASE.chargeFor(Duration.days(3))).toEqual(Price.euro(9.0));
        });

        it("should give 1 frequent renter point for 1 day", () => {
            expect(NEW_RELEASE.frequentRenterPointsFor(Duration.days(1))).toEqual(1);
        });

        it("should give 2 frequent renter points when more than 1 day", () => {
            fc.assert(fc.property(
                fc.nat().filter((v: number) => v >= 2),
                (days: number) => {
                    expect(NEW_RELEASE.frequentRenterPointsFor(Duration.days(days)))
                        .toEqual(2);
                }));
        });
    });
});
