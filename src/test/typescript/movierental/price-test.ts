import * as fc from 'fast-check';

import { Price } from '../../../main/typescript/movierental/price';

describe("Price should", () => {
    it("have equality", () => {
        fc.assert(
            fc.property(
                fc.integer(),
                (amount: number) => Price.equals(Price.euro(amount), Price.euro(amount))));
    });

    it("be addable", () => {
        fc.assert(
            fc.property(
                fc.integer(),
                fc.integer(),
                (left: number, right: number) => Price.equals(
                    Price.add(Price.euro(left), Price.euro(right)),
                    Price.euro(left + right))));
    });

    it("be scalable", () => {
        fc.assert(
            fc.property(
                fc.integer(),
                fc.integer(),
                (amount: number, quantity: number) => Price.equals(
                    Price.mul(Price.euro(amount), quantity),
                    Price.euro(amount * quantity))));
    });

    it("be renderable", () => {
        expect(Price.euro(2).render()).toEqual("2.0");
        expect(Price.euro(2.3).render()).toEqual("2.3");
        expect(Price.euro(2.43).render()).toEqual("2.4");
    });
});
