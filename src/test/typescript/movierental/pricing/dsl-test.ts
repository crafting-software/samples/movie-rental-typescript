import { Duration } from '../../../../main/typescript/movierental/duration';
import { Price } from '../../../../main/typescript/movierental/price';
import { parse } from '../../../../main/typescript/movierental/pricing/dsl';

describe("Pricing DSL", () => {

    it("should parse a pricing", () => {
        expect(priceFor("costs 2€ for 3 days then costs 3€ per day", Duration.days(5)))
            .toEqual(Price.euro(8.0));
    });

    it("should reject invalid pricing, invalid amount", () => {
        expect(() => parse("costs X€")).toThrow(Error);
    });

    it("should reject invalid pricing, invalid days", () => {
        expect(() => parse("costs 2€ for X days")).toThrow(Error);
    });

    it("should reject invalid pricing, invalid token", () => {
        expect(() => parse("costs 2€ for 3 DAIS")).toThrow(Error);
        expect(() => parse("costs 2€ FOP 3 days")).toThrow(Error);
    });

    function priceFor(content: string, duration: Duration): Price {
        return parse(content).chargeFor(duration);
    }
});
