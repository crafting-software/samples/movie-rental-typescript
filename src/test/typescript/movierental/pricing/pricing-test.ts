import { Duration } from '../../../../main/typescript/movierental/duration';
import { Price } from '../../../../main/typescript/movierental/price';
import { PriceDuration, Pricing, Rules } from '../../../../main/typescript/movierental/pricing/pricing';

describe("Flat rule", () => {
    function flatPriceFor(duration: Duration): Price {
        return Rules.flat(Price.euro(3)).priceFor(PriceDuration.of(duration)).total;
    }

    it("should return price regardless of days", () => {
        expect(flatPriceFor(Duration.days(1))).toEqual(Price.euro(3.0));
        expect(flatPriceFor(Duration.days(3))).toEqual(Price.euro(3.0));
        expect(flatPriceFor(Duration.days(10))).toEqual(Price.euro(3.0));
        expect(flatPriceFor(Duration.days(200))).toEqual(Price.euro(3.0));
    });
});

describe("Limited duration rule", () => {
    function limitedPriceFor(duration: Duration): Price {
        const rule = Rules.limited(Duration.days(4), Rules.proportional(Price.euro(3)));
        return rule.priceFor(PriceDuration.of(duration)).total;
    }

    it("should return inner costs with max duration", () => {
        expect(limitedPriceFor(Duration.days(1))).toEqual(Price.euro(3.0));
        expect(limitedPriceFor(Duration.days(2))).toEqual(Price.euro(6.0));
        expect(limitedPriceFor(Duration.days(3))).toEqual(Price.euro(9.0));
        expect(limitedPriceFor(Duration.days(4))).toEqual(Price.euro(12.0));
        expect(limitedPriceFor(Duration.days(5))).toEqual(Price.euro(12.0));
        expect(limitedPriceFor(Duration.days(10))).toEqual(Price.euro(12.0));
    });
});

describe("Proportional rule", () => {
    function proportionalPriceFor(duration: Duration): Price {
        return Rules.proportional(Price.euro(3)).priceFor(PriceDuration.of(duration)).total;
    }

    it("should return price per days", () => {
        expect(proportionalPriceFor(Duration.days(1))).toEqual(Price.euro(3.0));
        expect(proportionalPriceFor(Duration.days(3))).toEqual(Price.euro(9.0));
        expect(proportionalPriceFor(Duration.days(10))).toEqual(Price.euro(30.0));
    });
});

describe("Pricing", () => {
    const p = Pricing.of(Rules.limited(Duration.days(3), Rules.flat(Price.euro(2))))
        .then(Rules.proportional(Price.euro(3)));

    it("should apply rules", () => {
        expect(p.chargeFor(Duration.days(5))).toEqual(Price.euro(8.0));
    });

    it("should describe itself", () => {
        expect(p.describe()).toEqual("costs 2.0€ for 3 days then costs 3.0€ per day");
    });
});
