import * as fc from 'fast-check';

import { Duration } from '../../../main/typescript/movierental/duration';

describe("Duration should", () => {

    it("have equality", () => {
        fc.assert(
            fc.property(
                fc.integer(),
                (value: number) => Duration.equals(Duration.days(value), Duration.days(value))));
    });

    it("have min", () => {
        fc.assert(
            fc.property(
                fc.integer(), fc.integer(),
                (left: number, right: number) =>
                    Duration.equals(
                        Duration.min(Duration.days(left), Duration.days(right)),
                        Duration.days(Math.min(left, right)))));
    });
});
