import { Duration } from './duration';
import { Price } from './price';
import { PriceCode, PriceCodes } from './pricecode';

export class Movie {

    public static title(title: string): Movie {
        return new Movie(title, PriceCodes.Regular);
    }

    private constructor(
        private readonly _title: string,
        private readonly _priceCode: PriceCode) {
    }

    public get title(): string {
        return this._title;
    }

    public childrens(): Movie {
        return new Movie(this._title, PriceCodes.Childrens);
    }

    public newRelease(): Movie {
        return new Movie(this._title, PriceCodes.NewRelease);
    }

    public regular(): Movie {
        return new Movie(this._title, PriceCodes.Regular);
    }

    public chargeFor(duration: Duration): Price {
        return this._priceCode.chargeFor(duration);
    }

    public frequentRenterPointsFor(duration: Duration): number {
        return this._priceCode.frequentRenterPointsFor(duration);
    }
}
