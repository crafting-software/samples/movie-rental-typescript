import { Customer } from '../../../main/typescript/movierental/customer';
import { Duration } from '../../../main/typescript/movierental/duration';
import { Movie } from '../../../main/typescript/movierental/movie';
import { PriceCodes } from '../../../main/typescript/movierental/pricecode';
import { Rental } from '../../../main/typescript/movierental/rental';

describe("Customer", () => {

    const Madagascar = Movie.title("Madagascar").childrens();
    const Inception = Movie.title("Inception").regular();
    const Tides = Movie.title("Tides").newRelease();

    it("Statement for regular movie", () => {
        expect(statementFor(Customer.named("Sallie").add(rental(Inception, 3))))
            .toBe("Rental Record for Sallie\n" +
                "\tInception\t3.5\n" +
                "Amount owed is 3.5\n" +
                "You earned 1 frequent renter points");
    });

    it("Statement for new release movie", () => {
        expect(statementFor(Customer.named("Sallie").add(rental(Tides, 3))))
            .toBe("Rental Record for Sallie\n" +
                "\tTides\t9.0\n" +
                "Amount owed is 9.0\n" +
                "You earned 2 frequent renter points");
    });

    it("Statement for childrens movie", () => {
        expect(statementFor(Customer.named("Sallie").add(rental(Madagascar, 3))))
            .toBe("Rental Record for Sallie\n" +
                "\tMadagascar\t1.5\n" +
                "Amount owed is 1.5\n" +
                "You earned 1 frequent renter points");
    });

    it("Statement for many movies", () => {
        expect(statementFor(Customer.named("David")
            .add(rental(Madagascar, 6))
            .add(rental(Tides, 2))
            .add(rental(Inception, 8))))
            .toBe("Rental Record for David\n" +
                "\tMadagascar\t6.0\n" +
                "\tTides\t6.0\n" +
                "\tInception\t11.0\n" +
                "Amount owed is 23.0\n" +
                "You earned 4 frequent renter points");
    });

    function rental(movie: Movie, daysRented: number): Rental {
        return Rental.rent(movie).for(Duration.days(daysRented));
    }

    function statementFor(c: Customer): string {
        return c.statement();
    }
});
