import { Duration } from '../duration';
import { Price } from '../price';

export class PriceDuration {
    public static of(duration: Duration): PriceDuration {
        return new PriceDuration(Price.euro(0), duration);
    }

    private constructor(
        private readonly _accumulator: Price,
        private readonly _duration: Duration) {
    }

    public isExhausted(): boolean {
        return this._duration.days === 0;
    }

    public get total(): Price {
        return this._accumulator;
    }

    public consumeAll(on: (consumed: Duration) => Price): PriceDuration {
        return new PriceDuration(Price.add(this._accumulator, on(this._duration)), Duration.days(0));
    }

    public consume(duration: Duration, on: (consumed: Duration) => Price): PriceDuration {
        const consumed = Duration.min(this._duration, duration);
        const remaining = Duration.sub(this._duration, duration);
        return new PriceDuration(Price.add(this._accumulator, on(consumed)), remaining);
    }
}

export interface Rule {
    priceFor(input: PriceDuration): PriceDuration;

    describe(): string;
}

class FlatRule implements Rule {
    public constructor(private readonly _price: Price) {
    }

    public priceFor(input: PriceDuration): PriceDuration {
        return input.consumeAll((_) => this._price);
    }

    public describe(): string {
        return `costs ${this._price.render()}€`;
    }
}

class LimitedDurationRule implements Rule {
    public constructor(
        private readonly _duration: Duration,
        private readonly _inner: Rule) {
    }

    public priceFor(input: PriceDuration): PriceDuration {
        return input.consume(
            this._duration,
            (consumed) => this._inner.priceFor(PriceDuration.of(consumed)).total);
    }

    public describe(): string {
        return `${this._inner.describe()} for ${this._duration.days} days`;
    }
}

class ProportionalRule implements Rule {
    public constructor(private readonly _price: Price) {
    }

    public priceFor(input: PriceDuration): PriceDuration {
        return input.consumeAll((consumed) => Price.mul(this._price, consumed.days));
    }

    public describe(): string {
        return `costs ${this._price.render()}€ per day`;
    }
}

export class Rules {
    public static flat(price: Price): Rule {
        return new FlatRule(price);
    }

    public static proportional(price: Price): Rule {
        return new ProportionalRule(price);
    }

    public static limited(duration: Duration, rule: Rule): Rule {
        return new LimitedDurationRule(duration, rule);
    }
}

export class Pricing {
    public static of(rule: Rule): Pricing {
        return new Pricing([rule]);
    }

    private constructor(private readonly _rules: Rule[]) {
    }

    public then(rule: Rule): Pricing {
        return new Pricing([...this._rules, rule]);
    }

    public chargeFor(duration: Duration): Price {
        let result = PriceDuration.of(duration);
        for (const rule of this._rules) {
            result = rule.priceFor(result);
            if (result.isExhausted()) {
                break;
            }
        }
        return result.total;
    }

    public describe(): string {
        return this._rules.map((rule) => rule.describe()).join(" then ");
    }
}
