import { Duration } from '../duration';
import { Price } from '../price';
import { Pricing, Rule, Rules } from './pricing';

export function parse(content: string): Pricing {
    return new PricingParser(content).parse();
}

class PricingParser {
    private static AMOUNT: RegExp = new RegExp("^[0-9]*\\.?[0-9]+", "m");
    private static INT: RegExp = new RegExp("^[0-9]+", "m");
    private _position: number = 0;

    public constructor(private readonly _content: string) {
    }

    public parse(): Pricing {
        let result = Pricing.of(this.rule());
        while (this.check_token("then")) {
            result = result.then(this.rule());
        }
        if (this.eos()) {
            return result;
        }
        throw this.parseError("rule");
    }

    private rule(): Rule {
        this.token("costs");
        const amount = this.amount();
        let rule = Rules.flat(amount);
        if (this.check_token("per")) {
            this.token("day");
            rule = Rules.proportional(amount);
        }
        if (this.check_token("for")) {
            const value = this.integer();
            this.token("day");
            this.check_token("s");
            rule = Rules.limited(Duration.days(value), rule);
        }
        return rule;
    }

    private spaces(): void {
        while (!this.eos() && this.current() === " ") {
            this.advance(1);
        }
    }

    private amount(): Price {
        const matcher = PricingParser.AMOUNT.exec(this.tail());
        if (matcher !== null && matcher.index === 0) {
            this.advance(matcher[0].length);
            this.token("€");
            this.spaces();
            return Price.euro(parseFloat(matcher[0]));
        }
        throw this.parseError("amount");
    }

    private integer(): number {
        const matcher = PricingParser.INT.exec(this.tail());
        if (matcher !== null && matcher.index === 0) {
            this.advance(matcher[0].length);
            this.spaces();
            return parseInt(matcher[0], 10);
        }
        throw this.parseError("int");
    }

    private eos(): boolean {
        return this._position >= this._content.length;
    }

    private current(): string {
        return this._content.charAt(this._position);
    }

    private tail(): string {
        return this._content.substring(this._position);
    }

    private advance(count: number): void {
        this._position += count;
    }

    private token(token: string): void {
        if (this.tail().startsWith(token)) {
            this.advance(token.length);
        } else {
            throw this.parseError(token);
        }
        this.spaces();
    }

    private check_token(token: string): boolean {
        if (this.tail().startsWith(token)) {
            this.advance(token.length);
            this.spaces();
            return true;
        } else {
            return false;
        }
    }

    private parseError(token: string): Error {
        return new Error("Expected " + token + " at #" + this._position + " got '" + this.tail() + "\'");
    }
}
