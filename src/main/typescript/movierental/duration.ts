
export class Duration {
    public static days(value: number): Duration {
        return new Duration(value);
    }

    public static equals(left: Duration, right: Duration): boolean {
        return left.days === right.days;
    }

    public static min(left: Duration, right: Duration): Duration {
        return new Duration(Math.min(left.days, right.days));
    }

    public static sub(left: Duration, right: Duration): Duration {
        return new Duration(Math.max(left.days - right.days, 0));
    }

    private constructor(private readonly _value: number) {
    }

    public get days(): number {
        return this._value;
    }
}
