import { Duration } from './duration';
import { Movie } from './movie';
import { Price } from './price';

export class Rental {
    public static rent(movie: Movie): Rental {
        return new Rental(movie, Duration.days(0));
    }

    private constructor(private readonly _movie: Movie, private readonly _duration: Duration) {
    }

    public for(duration: Duration): Rental {
        return new Rental(this._movie, duration);
    }

    public get movieTitle(): string {
        return this._movie.title;
    }

    public charge(): Price {
        return this._movie.chargeFor(this._duration);
    }

    public frequentRenterPoints(): number {
        return this._movie.frequentRenterPointsFor(this._duration);
    }
}
